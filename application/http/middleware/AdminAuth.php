<?php

namespace app\http\middleware;

use think\facade\{Request,Session,Cookie,Config,Url};
use app\model\{AuthGroup, SysAdmin, SysMenu, SysAuth};
use think\Response;

class AdminAuth
{

    private $controller;

    private $action;


    /**
     * @param object $request
     * @param \Closure $next
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function handle($request, \Closure $next)
    {

        $this->controller = strtolower($request->controller());

        $this->action = strtolower($request->action());

        //检测是否需要登录
        if ($this->match(Config::get('admin_auth.noNeedLogin')) === false) {

            $admin = $this->isLogin();

            if ($admin === false) {

                return $this->error("请登录", 'account/login');

            }

            if ($this->match(Config::get('admin_auth.noNeedRight')) === false) {

                if ($admin['group'] != 0){

                    if ($this->checkAuth($admin['group']) === false) {

                        return $this->error("无权访问", 'index/frame');

                    }

                }

            }

        }

        return $next($request);

    }


    /**
     * @param mixed $match
     *
     * @return bool
     */
    private function match($match)
    {

        $match = is_array($match) ? $match : explode(',', $match);

        if (!$match) {
            return false;
        }

        $match = array_map('strtolower', $match);

        //规则匹配
        return $this->analyzeAuth($match);
    }

    /**
     * @return bool|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    private function isLogin()
    {
        $admin = Session::get('admin');

        if (empty($admin)) {

            if (Cookie::has('adminEncrypt')) {

                $token = Cookie::get('adminEncrypt');

                $admin = SysAdmin::get([
                    'token' => $token
                ]);

                if (empty($admin)) {

                    return false;

                }

                Session::set('menu', SysMenu::buildMenu($admin['group']));

                Session::set('admin', $admin->toArray());

                return $admin;

            } else {

                return false;

            }
        }

        return $admin;

    }

    /**
     * @param int $group
     * @return bool
     */
    private function checkAuth($group)
    {

        $group = AuthGroup::findOrEmpty($group);

        $auth = explode(',', $group['auth']);

        $authList = SysAuth::all($auth);

        $routes = [];

        foreach ($authList as $item) {

            array_push($routes, strtolower("{$item->controller}/{$item->action}"));

        }

        if (in_array("{$this->controller}/$this->action", $routes)) {
            return true;
        }

        return false;

    }

    /**
     * @param array $arr
     * @return string
     */
    private function analyzeAuth($arr = [])
    {
        foreach ($arr as $item) {
            [$controller, $action] = explode('/', $item);
            if ($this->controller == $controller) {
                if ($action == '*' || $this->action == $action) {
                    return true;
                }
            }
        }
        return false;
    }

	/**
	 * 操作错误跳转的快捷方法
	 *
	 * @access protected
	 *
	 * @param mixed   $msg    提示信息
	 * @param string  $url    跳转的URL地址
	 * @param mixed   $data   返回的数据
	 * @param integer $wait   跳转等待时间
	 * @param array   $header 发送的Header信息
	 *
	 * @return Response
	 */
    private function error($msg = '', $url = null, $data = '', $wait = 3, array $header = [])
    {
        $type = $this->getResponseType();
        if (is_null($url)) {
            $url = Request::isAjax() ? '' : 'javascript:history.back(-1);';
        } elseif ('' !== $url) {
            $url = (strpos($url, '://') || 0 === strpos($url, '/')) ? $url : Url::build($url);
        }

        $result = [
            'code' => 0,
            'msg' => $msg,
            'data' => $data,
            'url' => $url,
            'wait' => $wait,
        ];

        if ('html' == strtolower($type)) {
            $type = 'jump';
        }

		return Response::create($result, $type)->header($header)->options(['jump_template' => Config::get('dispatch_error_tmpl')]);
    }

    /**
     * 获取当前的response 输出类型
     * @access protected
     * @return string
     */
    protected function getResponseType()
    {
        $isAjax = Request::isAjax();
        return $isAjax
            ? Config::get('default_ajax_return')
            : Config::get('default_return_type');
    }

}
