<?php


namespace app\http\middleware;


use think\facade\{Config,Response};
use app\model\User;
use traits\ErrCode;

class ApiAuth
{

	use ErrCode;

    private $controller;

    private $action;

    /**
     * @param object $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {

        $this->controller = strtolower($request->controller());

        $this->action = strtolower($request->action());

        $token = $request->{strtolower($request->method())}('token');

        //检测是否需要登录
        $member = $this->isLogin($token);

        if ($member === false) {

            if ($this->match(Config::get('api_auth.noNeedLogin')) === false) {

                return Response::create([
                    'code' => self::$notLogin,
                    'msg' => '请登录',
                    'data' => [],
                ], 'json', 200);

            }

        }

        $request->u = is_object($member) ? $member->toArray() : false;

        return $next($request);

    }

    /**
     *
     * @param mixed $match
     *
     * @return bool
     */
    private function match($match)
    {

        $match = is_array($match) ? $match : explode(',', $match);

        if (!$match) {
            return false;
        }

        $match = array_map('strtolower', $match);

        //规则匹配
        return $this->analyzeAuth($match);
    }

    /**
     * @param $token
     * @return bool|mixed
     */
    private function isLogin($token)
    {
        if (empty($token)) {
            return false;
        }
        //尝试根据Token查找用户 失败返回false 成功返回用户对象
        try {
            $data = User::where('token', $token)->findOrEmpty();
        } catch (\Exception $e) {
            return false;
        }

        if ($data->isEmpty()) {
            return false;
        }

        return $data;
    }

    /**
     * @param array $arr
     * @return string
     */
    private function analyzeAuth($arr = [])
    {
        foreach ($arr as $item) {
            [$controller, $action] = explode('/', $item);
            if ($this->controller == $controller) {
                if ($action == '*' || $this->action == $action) {
                    return true;
                }
            }
        }
        return false;
    }
}