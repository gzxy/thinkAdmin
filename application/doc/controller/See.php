<?php


namespace app\doc\controller;


use facade\Document;
use think\Controller;
use think\facade\Env;

class See extends Controller
{

    public function api()
    {

        try{

            $docArr = Document::options([
                'api_dir' => Env::get('app_path') . 'api/controller',
                'project_name' => '测试',
                'host' => 'http://127.0.0.1' //换成本地可访问地址或线上地址
            ])->makeDocRule();

        }catch (\Exception $e){

            $this->error($e->getMessage());

        }

        $docArr = $docArr ?? [];

        $count = 0;

        foreach ($docArr['doc'] as $item){
            $count += count($item);
        }

        $this->assign('docArr',$docArr)->assign('count',$count);

        return $this->fetch('/doc');

    }

}