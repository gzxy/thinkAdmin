<?php

function getParams($params)
{
    if (empty($params)) return false;
    $jsonArr = [];
    foreach ($params as $key => $value) {
        array_push($jsonArr, $value['name'] . ":''");
    }
    $paramsJson = join(",", $jsonArr);

    return '{' . $paramsJson . '}';
}