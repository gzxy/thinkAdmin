<?php

namespace app\api\controller;

use traits\Base;
use facade\Sms;

class Index
{

	use Base;

    /**
     * 接口文档注释示范
     *
     * @url    /web/index/index
     * @method POST
     *
     * @param string mobile 手机号
     * @return string|\think\response
     */
	public function index ()
	{

		$this->import('mobile', self::$get['mobile'] ?? '');

		return $this->output('请求成功', 1);
	}

    /**
     * 短信示例
     *
     * @url    /web/index/sms
     * @method POST
     *
     * @param string mobile 手机号
     * @return string|\think\response
     */
	public function sms ()
	{
		$data = self::$get;

		$rule = [
			'mobile|手机号' => 'require|mobile'
		];

		$result = $this->validate($data, $rule);

		if ($result !== true) {

			return $this->output($result, self::$validateErr);

		}

		//		[
		//			$code,
		//			$msg
		//		] = Sms::send_sms([
		//			'mobile' => $data['mobile'],
		//			'accessKeyId' => Config::get('sms.accessKeyId'),
		//			'accessKeySecret' => Config::get('sms.accessKeySecret'),
		//			'tpl' => Config::get('sms.TemplateCode'),
		//			'SignName' => Config::get('sms.SignName'),
		//			'data' => [
		//				'code' => generate_code()
		//			]
		//		], 'dayu', 120);

		[
			$code,
			$msg
		] = Sms::sendSms([
			'mobile' => $data['mobile'],
			'tpl' => 'test',
			'data' => [
				'code' => generate_code()
			]
		], 'test', 120);

		$this->import('expire_time', 120);

		return $this->output($msg, $code);
	}
}