<?php
return [
    'noNeedLogin' => [
        'account/login'
    ],
    'noNeedRight' => [
        'index/*',
        'widget/*',
        'account/logout'
    ]
];