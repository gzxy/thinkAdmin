<?php
return [
    'setting' => [
        'title' => '系统设置',
        'link' => 'config/settings',
        'icon' => '',
        'fid' => 'root'
    ],
    'meun' => [
        'title' => '菜单管理',
        'link' => 'menu/index',
        'icon' => '',
        'fid' => 'root'
    ],
    'admin' => [
        'title' => '管理员管理',
        'link' => 'admin/index',
        'icon' => '',
        'fid' => 'root'
    ],
    'group' => [
        'title' => '权限组管理',
        'link' => 'group/index',
        'icon' => '',
        'fid' => 'root'
    ],
    'wechat' => [
        'title' => '微信设置',
        'link' => 'config/wechat',
        'icon' => '',
        'fid' => 'root'
    ],
    'alipay' => [
        'title' => '支付宝设置',
        'link' => 'config/alipay',
        'icon' => '',
        'fid' => 'root'
    ],
    'sms' => [
        'title' => '短信配置',
        'link' => 'config/sms',
        'icon' => '',
        'fid' => 'root'
    ],
    'error' => [
        'title' => '错误日志',
        'link' => 'error/index',
        'icon' => '',
        'fid' => 'root'
    ]
];