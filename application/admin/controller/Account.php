<?php

namespace app\admin\controller;

use think\facade\{Cookie, Session};
use app\model\SysAdmin;
use traits\Admin;

class Account
{

	use Admin;

	/**
	 * 管理员登录
	 * @return string|\think\response
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function login ()
	{

		if (self::$request->isPost()) {

			$result = $this->validate(self::$post, 'app\admin\validate\Login');

			if ($result !== true) {

				return $this->output($result, self::$validateErr);

			}

			$model = new SysAdmin;

			[
				$state,
				$msg,
				$url
			] = $model->login(self::$post);

			return $this->{$state}($msg, $url);

		}

		if (Cookie::has('adminEncrypt')) {

			try {

				$admin = SysAdmin::where('token', Cookie::get('adminEncrypt'))->findOrEmpty();

			} catch (\Exception $e) {

				return $this->output();

			}

			if ($admin->isEmpty()) {

				return $this->output();

			}

			return $this->success("您已登录，正在前往主页", 'index/index');

		}

		return $this->output();

	}

	public function logout ()
	{

		Session::delete('admin');

		Session::delete('menu');

		Cookie::delete('adminEncrypt');

		return $this->success('退出成功', 'account/login');

	}

}