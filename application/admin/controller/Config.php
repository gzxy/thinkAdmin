<?php

namespace app\admin\controller;

use form\FormBuilder as Form;
use think\facade\Url;
use traits\Admin;

class Config
{

	use Admin;

	public static $description = "系统设置";

	/**
	 * @title 系统设置
	 */
	public function settings ()
	{

		if (self::$request->isAjax()) {

			$settings = var_export(self::$post, true);

			file_put_contents('../config/settings.php', '<?php' . PHP_EOL . 'return ' . $settings . ';');

			return $this->success('保存成功', self::$ok);

		}

		$data = \think\facade\Config::get('settings.');

		$field = [
			Form::input('icp', '备案号', $data['icp'] ?? ''),
			Form::input('title', '网站标题', $data['title'] ?? ''),
			Form::input('workTime', '办公时间', $data['workTime'] ?? ''),
			Form::input('teamwork', '商务沟通', $data['teamwork'] ?? ''),
			Form::input('address', '办公地址', $data['address'] ?? ''),
			Form::textarea('keywords', '网站关键词', $data['keywords'] ?? ''),
			Form::input('company_name', '公司名称', $data['company_name'] ?? '')
		];

		$form = Form::make_post_form("系统设置", $field, Url::build('settings'), 1);

		$this->import('form', $form);

		return $this->output(self::$formTpl);
	}

	/**
	 * @title 微信设置
	 */
	public function wechat ()
	{

		if (self::$request->isAjax()) {

			$settings = var_export(self::$post, true);

			file_put_contents('../config/wechat.php', '<?php' . PHP_EOL . 'return ' . $settings . ';');

			return $this->success('保存成功', self::$ok);

		}

		$data = \think\facade\Config::get('wechat.');

		$field = [
			Form::input('appId', '微信appid', $data['appId'] ?? ''),
			Form::input('appSecret', 'appSecret', $data['appSecret'] ?? ''),
			Form::input('mchid', '商户号', $data['mchid'] ?? ''),
			Form::input('key', '支付密钥', $data['key'] ?? '')

		];

		$form = Form::make_post_form('微信配置', $field, Url::build('wechat'), 1);

		$this->import('form', $form);

		return $this->output(self::$formTpl);
	}

	public function alipay ()
	{

		if (self::$request->isAjax()) {

			$settings = var_export(self::$post, true);

			file_put_contents('../config/alipay.php', '<?php' . PHP_EOL . 'return ' . $settings . ';');

			return $this->success('保存成功', self::$ok);

		}

		$data = \think\facade\Config::get('alipay.');

		$field = [
			Form::input('app_id', '支付宝appId', $data['app_id'] ?? ''),
			Form::input('ali_public_key', '支付宝公钥', $data['ali_public_key'] ?? ''),
			Form::input('private_key', '商户私钥', $data['private_key'] ?? '')

		];

		$form = Form::make_post_form('支付宝配置', $field, Url::build('alipay'), 1);

		$this->import('form', $form);

		return $this->output(self::$formTpl);

	}

	/**
	 * @title 短信设置
	 */
	public function sms ()
	{

		if (self::$request->isAjax()) {

			$settings = var_export(self::$post, true);

			file_put_contents('../config/sms.php', '<?php' . PHP_EOL . 'return ' . $settings . ';');

			return $this->success('保存成功', self::$ok);

		}

		$data = \think\facade\Config::get('sms.');

		$field = [
			Form::input('accessKeyId', '阿里密钥ID', $data['accessKeyId'] ?? ''),
			Form::input('accessKeySecret', '阿里密钥', $data['accessKeySecret'] ?? ''),
			Form::input('TemplateCode', '阿里短信模板ID', $data['TemplateCode'] ?? ''),
			Form::input('SignName', '阿里短信签名', $data['SignName'] ?? '')

		];

		$form = Form::make_post_form('短信配置', $field, Url::build('sms'), 1);

		$this->import('form', $form);

		return $this->output(self::$formTpl);
	}

}