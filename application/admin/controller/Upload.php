<?php


namespace app\admin\controller;

use FormBuilder\Json;
use traits\Admin;

class Upload
{

	use Admin;

    public static $description = "文件上传";

    /**
     * @title 上传图片
     */
    public function image(){

        $image = self::$request->file('img');

        if ($image){

            $uploadInfo = $image->rule('sha1')->validate([
                'size' => 5242880, //5M
                'ext' => 'jpg,jpeg,png,gif'
            ])->move('./uploads');

            if ($uploadInfo){

                return Json::uploadSucc("/uploads/{$uploadInfo->getSaveName()}",'上传成功');

            }else{

                return Json::uploadFail($image->getError());

            }

        }

        return Json::uploadFail('未找到要上传的图片');

    }

    /**
     * @title 上传文件
     */
    public function file(){

        $file = self::$request->file('file');

        if ($file){

            $uploadInfo = $file->rule('sha1')->validate([
                'size' => 5242880, //5M
                'ext' => 'mp4,zip,mp3,pdf'
            ])->move('./uploads');

            if ($uploadInfo){

                return Json::uploadSucc("/uploads/{$uploadInfo->getSaveName()}",'上传成功');

            }else{

                return Json::uploadFail($file->getError());

            }

        }

        return Json::uploadFail('未找到要上传的文件');

    }

}