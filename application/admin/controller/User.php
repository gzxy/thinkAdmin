<?php

namespace app\admin\controller;

use app\model\User as Model;
use form\FormBuilder;
use think\facade\Url;
use traits\Admin;

class User
{

	use Admin;

	public static $description = '用户管理';

	/**
	 * @title 用户列表
	 */
	public function index ()
	{

		if (self::$request->isAjax()) {

			$limit = self::$get['limit'] ?? 30;

			$field = self::$get['field'] ?? 'id';

			$order = self::$get['order'] ?? 'desc';

			$searchField = [];

			$searchData = [];

			if (!empty(self::$get['create_time'] ?? '')) {

				$create_time = explode('-', self::$get['create_time']);

				array_map('strtotime', $create_time);

				$searchField[] = 'create_time';

				$searchData['create_time'] = $create_time;

			}

			if (!empty(self::$get['mobile'] ?? '')) {

				$searchField[] = 'mobile';

				$searchData['mobile'] = self::$get['mobile'];

			}

			if (!empty(self::$get['nickname'] ?? '')) {

				$searchField[] = 'nickname';

				$searchData['nickname'] = self::$get['nickname'];

			}

			try {

				$data = Model::with('parent')->withSearch($searchField, $searchData)->order($field, $order)->paginate($limit)->each(function ($item) {

					$item['handle'] = [
						[
							'name' => '修改',
							'href' => Url::build('edit', ['id' => $item['id']]),
							'event_type' => 'url',
							'btn_type' => 'btn-success'
						],
						[
							'name' => '删除',
							'href' => Url::build('destory', ['id' => $item['id']]),
							'event_type' => 'confirm',
							'btn_type' => 'btn-danger',
							'msg' => '确认删除此用户？'
						]
					];

				});

			} catch (\Exception $e) {

				return $this->layTable([], $e->getMessage());

			}

			return $this->layTable($data);

		}

		return $this->output();

	}

	/**
	 * @title 创建用户
	 */
	public function create ()
	{

		if (self::$request->isAjax()) {

			$data = self::$post;

			$rule = [
				'mobile' => 'require|mobile|unique:user'
			];

			$result = $this->validate($data, $rule);

			if ($result !== true) {

				return $this->output($result, 0);

			}

			if (empty($data['nickname']))
				$data['nickname'] = substr_replace(self::$post, '****', 3, 4);

			$data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);

			$result = Model::create($data);

			if ($result) {

				$inviteCode = inviteCode($result['id']);//生成邀请码

				Model::update([
					'invite_code' => $inviteCode
				], [
					'id' => $result['id']
				]);

				return $this->output('用户创建成功');

			}

			return $this->output('用户创建失败', self::$fail);

		}

		$field = [
			FormBuilder::input('mobile', '手机号')->required('请填写用户登录手机号'),
			FormBuilder::password('password', '密码')->required('请输入用户登录密码'),
			FormBuilder::input('nickname', '昵称'),
			FormBuilder::uploadImageOne('avatar', '头像', Url::build('upload/image'))->name('img'),
			FormBuilder::radio('sex', '性别', 0)->options([
				[
					'value' => 0,
					'label' => '未知'
				],
				[
					'value' => 1,
					'label' => '男'
				],
				[
					'value' => 2,
					'label' => '女'
				]
			]),
			FormBuilder::selectOne('parent_id', '上级用户')->setOptions(function () {
				$user = Model::select();
				$label = [];
				foreach ($user as $item) {
					array_push($label, [
						'value' => $item['id'],
						'label' => "{$item['mobile']}-{$item['nickname']}"
					]);
				}
				return $label;
			})
		];

		$form = FormBuilder::make_post_form('创建用户', $field, Url::build('create'), 2);

		$this->import('form', $form);

		return $this->output(self::$formTpl);

	}

	/**
	 * @title 修改用户
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function edit (int $id)
	{

		$user = Model::findOrEmpty($id);

		if (self::$request->isAjax()) {

			if ($user->isEmpty()) {

				return $this->output('用户不存在', self::$fail);

			}

			$data = self::$post;

			$rule = [
				'mobile' => 'require|mobile|unique:user,mobile,' . $id
			];

			$result = $this->validate($data, $rule);

			if ($result !== true) {

				return $this->output($result, 0);

			}

			if (empty($data['nickname']))
				$data['nickname'] = substr_replace(self::$post['mobile'], '****', 3, 4);

			$data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);

			$user->data($data);

			if ($user->save()) {

				return $this->output('用户修改成功');

			}

			return $this->output('用户修改失败', self::$fail);

		}

		$field = [
			FormBuilder::hidden('id', $id),
			FormBuilder::input('mobile', '手机号', $user['mobile'])->required('请填写用户登录手机号'),
			FormBuilder::password('password', '密码')->placeholder('不修改密码则不需要填写'),
			FormBuilder::input('nickname', '昵称', $user['nickname']),
			FormBuilder::uploadImageOne('avatar', '头像', Url::build('upload/image'), $user['avatar'])->name('img'),
			FormBuilder::radio('sex', '性别', $user->getData('sex'))->options([
				[
					'value' => 0,
					'label' => '未知'
				],
				[
					'value' => 1,
					'label' => '男'
				],
				[
					'value' => 2,
					'label' => '女'
				]
			]),
			FormBuilder::selectOne('parent_id', '上级用户', $user['parent_id'])->setOptions(function () use ($id) {
				$user = Model::where('id', '<>', $id)->select();
				$label = [];
				foreach ($user as $item) {
					array_push($label, [
						'value' => $item['id'],
						'label' => "{$item['mobile']}-{$item['nickname']}"
					]);
				}
				return $label;
			})
		];

		$form = FormBuilder::make_post_form('修改用户', $field, Url::build('edit'), 2);

		$this->import('form', $form);

		return $this->output(self::$formTpl);

	}

}