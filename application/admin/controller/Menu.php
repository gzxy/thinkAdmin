<?php

namespace app\admin\controller;

use think\facade\{Session, Url};
use form\FormBuilder as Form;
use app\model\SysMenu;
use traits\Admin;

class Menu
{

	use Admin;

	public static $description = "菜单管理";

	/**
	 * @title 菜单列表
	 */
	public function index ()
	{

		if (self::$request->isAjax()) {

			$menu = SysMenu::getMenuSelect();

			foreach ($menu as &$item) {

				$item['handle'] = [
					[
						'name' => '修改',
						'href' => Url::build('edit', ['id' => $item['id']]),
						'event_type' => 'url',
						'btn_type' => 'btn-success'
					],
					[
						'name' => '删除',
						'href' => Url::build('destory', ['id' => $item['id']]),
						'event_type' => 'confirm',
						'btn_type' => 'btn-danger'
					]
				];

			}

			return $this->layTable($menu);

		}

		return $this->output();

	}

	/**
	 * @title 创建菜单
	 * @throws \think\db\exception\DataNotFoundException;
	 * @throws \think\db\exception\ModelNotFoundException;
	 * @throws \think\exception\DbException;
	 */
	public function create ()
	{
		if (self::$request->isAjax()) {

			$vaildate = SysMenu::valititle(self::$post['title']);

			if ($vaildate === false) {

				return $this->output('菜单标题已存在！', 0);

			}

			if (!empty(self::$post['link'])) {

				[
					$controller,
					$action
				] = explode('/', self::$post['link']);

				self::$post['controller'] = $controller;

			}

			if (SysMenu::create(self::$post)) {

				Session::set('menu', SysMenu::buildMenu(self::$g['admin']['group']));

				return $this->output('创建成功');

			}

			return $this->output('创建失败', 0);

		}

		$field = [
			Form::input('title', '菜单标题')->col(8)->required('标题不能为空'),
			Form::selectOne('fid', '父级id', 0)->setOptions(function () {
				$list = SysMenu::getMenuSelect();
				$menus = [
					[
						'value' => 0,
						'label' => '顶部菜单'
					]
				];
				foreach ($list as $menu) {
					$menus[] = [
						'value' => $menu['id'],
						'label' => $menu['title']
					];
				}
				return $menus;
			})->filterable(1)->required('请选择父级菜单'),
			Form::frameInputOne('icon', '图标', url('widget/icon'))->frameTitle('请选择菜单图标')->width('70%')->height('500px'),
			Form::input('link', '菜单链接')->placeholder('控制器/方法名'),
			Form::input('desc', '菜单描述')->type('textarea'),
		];

		$form = Form::make_post_form('添加菜单', $field, Url::build('create'), 2);
		$this->import('form', $form);
		return $this->output(self::$formTpl);
	}

	/**
	 * @title 编辑菜单
	 *
	 * @param int $id
	 *
	 * @return string|\think\response
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function edit (int $id)
	{

		if (self::$request->isAjax()) {

			$vaildate = SysMenu::valititle(self::$post['title'], $id);

			if ($vaildate === false) {

				return $this->output('菜单标题已存在！', self::$fail);

			}

			if (SysMenu::update(self::$post)) {

				Session::set('menu', SysMenu::buildMenu(self::$g['admin']['group']));

				return $this->output('保存成功！');

			}

			return $this->output('保存失败！', self::$fail);

		}

		$menu = SysMenu::get($id);

		$field = [
			Form::hidden('id', $menu->id),
			Form::input('title', '菜单标题', $menu->title)->col(8)->required('标题不能为空'),
			Form::selectOne('fid', '父级id', $menu->fid)->setOptions(function () {
				$list = SysMenu::getMenuSelect();
				$menus = [
					[
						'value' => 0,
						'label' => '顶部菜单'
					]
				];
				foreach ($list as $menu) {
					$menus[] = [
						'value' => $menu['id'],
						'label' => $menu['title']
					];
				}
				return $menus;
			})->filterable(1)->required('请选择父级菜单'),
			Form::frameInputOne('icon', '图标', url('widget/icon'), $menu->icon)->frameTitle('请选择菜单图标')->width('70%')->height('500px'),
			Form::input('link', '菜单链接', $menu->link)->placeholder('控制器/方法名'),
			Form::input('desc', '菜单描述', $menu->desc)->type('textarea'),
		];

		$form = Form::make_post_form('编辑菜单', $field, Url::build('edit'), 2);
		$this->import('form', $form);
		return $this->output(self::$formTpl);

	}

	/**
	 * @title 删除菜单
	 *
	 * @param int $id
	 *
	 * @return string|\think\response
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
	public function destory (int $id)
	{

		self::$api = true;

		$menu = SysMenu::get($id);

		if (empty($menu)) {

			return $this->output('菜单不存在', self::$fail);

		}

		$menu->delete();

		SysMenu::buildMenu(self::$g['admin']['group']);

		return $this->output('删除成功');
	}

}