<?php

namespace app\admin\controller;

use app\model\{AuthGroup, SysAuth};
use form\FormBuilder;
use think\facade\Url;
use traits\Admin;

class Group
{

	use Admin;

	/**
	 * @title 权限组列表
	 * @return array|mixed
	 */
	public function index ()
	{

		if (self::$request->isAjax()) {

			$limit = self::$get['limit'] ?? 30;

			$field = self::$get['field'] ?? 'id';

			$order = self::$get['order'] ?? 'desc';

			$searchField = [];

			$searchData = [];

			$create_time = self::$get['create_time'] ?? '';

			if (!empty($create_time)) {

				$create_time = explode('-', self::$get['create_time']);

				array_map('strtotime', $create_time);

				$searchField[] = 'create_time';

				$searchData['create_time'] = $create_time;

			}

			$name = self::$get['name'] ?? '';

			if (!empty($name)) {

				$searchField[] = 'name';

				$searchData['name'] = $name;

			}

			try {

				$data = AuthGroup::withSearch($searchField, $searchData)->order($field, $order)->paginate($limit)->each(function ($item) {
					$item['handle'] = [
						[
							'name' => '修改',
							'href' => Url::build('edit', ['id' => $item['id']]),
							'event_type' => 'url',
							'btn_type' => 'btn-success'
						],
						[
							'name' => '删除',
							'href' => Url::build('destory', ['id' => $item['id']]),
							'event_type' => 'confirm',
							'btn_type' => 'btn-danger',
							'msg' => '确认删除此用户组？'
						]
					];

				});

			} catch (\Exception $e) {

				return $this->layTable([], $e->getMessage());

			}

			return $this->layTable($data);

		}

		return $this->output();

	}

	/**
	 * @title 创建权限组
	 * @return mixed
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 * @throws \Exception
	 */
	public function create ()
	{

		if (self::$request->isAjax()) {

			self::$post['auth'] = implode(',', self::$post['auth']);

			if (AuthGroup::create(self::$post)) {

				return $this->output('用户组创建成功');

			}

			return $this->output('用户组创建失败', self::$fail);

		}

		$data = [];

		$ignore = [
			'Base',
			'Account',
			'Index',
			'Admin',
			'Menu',
			'Widget',
			'Config',
			'Exception',
			'Error',
			'Group'
		];

		$crts = array_diff(getController(), $ignore);

		foreach ($crts as $v) {

			$cl = 'app\admin\controller' . '\\' . $v;

			try {
				$ref = new \ReflectionClass(new  $cl());
				$desc = $ref->getProperty('description')->getValue();
			} catch (\Exception $e) {
				throw new \Exception("{$v}未设置简介");
			}

			$v = strtolower($v);

			$acts = getActions($v);

			$parent = [
				'controller' => $v,
				'desc' => $desc,
				'fid' => 0
			];

			try {
				foreach ($acts as $vv) {
					$parent['children'][] = [
						'controller' => $v,
						'action' => $vv,
						'desc' => getActionNote($v, $vv)
					];
				}
			} catch (\Exception $e) {
				throw new \Exception("{$v} - {$vv}未设置标题");
			}

			$data[] = $parent;
		}

		SysAuth::buildMenu($data);

		$treeData = [];

		$auth = SysAuth::where('fid', 0)->all();

		foreach ($auth as $v) {

			$childAuth = SysAuth::where('fid', $v->id)->all();

			$children = [];

			foreach ($childAuth as $vv) {

				$children[] = FormBuilder::treeData($vv->id, $vv->desc);

			}

			$treeData[] = FormBuilder::treeData($v->id, $v->desc)->children($children);

		}

		$field = [
			FormBuilder::input('name', '用户组名称')->required('请填写用户组名称'),
			FormBuilder::treeChecked('auth', '权限管理')->data($treeData)->required('请选择用户组权限')
		];

		$form = FormBuilder::make_post_form('权限管理', $field, url('create'), 2);

		$this->import('form', $form);

		return $this->output(self::$formTpl);

	}

	/**
	 * @title 修改权限组
	 *
	 * @param int $id
	 *
	 * @return mixed
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 * @throws \Exception
	 */
	public function edit (int $id)
	{

		$group = AuthGroup::findOrEmpty($id);

		if (self::$request->isAjax()) {

			if ($group->isEmpty()) {

				return $this->output('该用户组不存在', self::$fail);

			}

			self::$post['auth'] = implode(',', self::$post['auth']);

			$group->data(self::$post, true);

			if ($group->save()) {

				return $this->output('用户组修改成功');

			}

			return $this->output('用户组修改失败', self::$fail);

		}

		$data = [];

		$ignore = [
			'Base',
			'Account',
			'Index',
			'Admin',
			'Menu',
			'Widget',
			'Config',
			'Exception',
			'Error',
			'Group'
		];

		$crts = array_diff(getController(), $ignore);

		foreach ($crts as $v) {

			$cl = 'app\admin\controller' . '\\' . $v;

			try {
				$ref = new \ReflectionClass(new  $cl());
				$desc = $ref->getProperty('description')->getValue();
			} catch (\Exception $e) {
				throw new \Exception("{$v}未设置简介");
			}

			$v = strtolower($v);

			$acts = getActions($v);

			$parent = [
				'controller' => $v,
				'desc' => $desc,
				'fid' => 0
			];

			try {
				foreach ($acts as $vv) {
					$parent['children'][] = [
						'controller' => $v,
						'action' => $vv,
						'desc' => getActionNote($v, $vv)
					];
				}
			} catch (\Exception $e) {
				throw new \Exception("{$v} - {$vv}未设置标题");
			}

			$data[] = $parent;
		}

		SysAuth::buildMenu($data);

		$treeData = [];

		$auth = SysAuth::where('fid', 0)->all();

		foreach ($auth as $v) {

			$childAuth = SysAuth::where('fid', $v->id)->all();

			$children = [];

			foreach ($childAuth as $vv) {

				$children[] = FormBuilder::treeData($vv->id, $vv->desc);

			}

			$treeData[] = FormBuilder::treeData($v->id, $v->desc)->children($children);

		}

		$field = [
			FormBuilder::input('name', '用户组名称', $group['name'])->required('请填写用户组名称'),
			FormBuilder::treeChecked('auth', '权限管理', explode(',', $group['auth']))->data($treeData)->required('请选择用户组权限')
		];

		$form = FormBuilder::make_post_form('权限管理', $field, url('edit'), 2);

		$this->import('form', $form);

		return $this->output(self::$formTpl);

	}

	/**
	 * @title 删除权限组
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function destory (int $id)
	{

		self::$api = true;

		$group = AuthGroup::findOrEmpty($id);

		if ($group->isEmpty()) {

			return $this->output('该用户组不存在', self::$fail);

		}

		$group->delete();

		$this->import('url', Url::build('group/index'));

		return $this->output('删除成功，如该用户组下有管理员，请重新分配用户组');

	}

}