<?php


namespace app\admin\controller;

use app\model\ExceptionLog;
use traits\Admin;

class Error
{

	use Admin;

    public function index(){

        if (self::$request->isAjax()){
            $limit = $this->get['limit'] ?? 30;
            $field = $this->get['field'] ?? 'id';
            $order = $this->get['order'] ?? 'desc';
            try{
                $data = ExceptionLog::order($field, $order)->paginate($limit);
            }catch (\Exception $e){
                return $this->layTable([],$e->getMessage());
            }
            return $this->layTable($data);
        }

        return $this->output();
    }

    public function detail(int $id)
    {

        $log = ExceptionLog::findOrEmpty($id)->toArray();

        $this->import('log',$log);

        return $this->output();
    }
    
}