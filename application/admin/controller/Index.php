<?php

namespace app\admin\controller;

use think\Db;
use think\facade\Url;
use think\helper\Time;
use traits\Admin;

class Index
{

	use Admin;

    public function index()
    {

        $this->import('menu', $this->buildMenuTpl(self::$g['menu']))
            ->import('admin', self::$g['admin'])
            ->import('title', self::$g['title']);

        return $this->output();

    }

    public function frame()
    {

        [$dayStart, $dayEnd] = Time::today();

        $this->import('today_member', Db::name('user')->whereBetweenTime('create_time', $dayStart, $dayEnd)->count())
            ->import('all_member', Db::name('user')->count());

        return $this->output();

    }

    private function buildMenuTpl($menu)
    {

        if (empty($menu)) return '';

        $html = '';

        foreach ($menu as $v) {

            $link = empty($v['link']) ? '' : Url::build($v['link']);

            $icon = $item['icon'] ?? 'fa fa-circle-o';

            $router = str_replace('/', '-', $v['link']);

            if (($v['fid'] ?? 0) === 0) {

                $html .= <<<MENU
<li class='treeview'><a data-frame='{$link}'  data-router='{$router}'><i class='{$icon}'></i><span>{$v['title']}</span><span class='pull-right-container'>
MENU;

                if (!empty($v['children'])) {
                    $html .= '<i class=\'fa fa-angle-left pull-right\'></i>';
                }

                $html .= '</span></a>';
            } else {
                $html .= <<<MENU
<li><a data-frame='{$link}' data-router='{$router}'><i class='{$icon}'></i><span>{$v['title']}</span></a></li>
MENU;
            }

            if (!empty($v['children'])) {

                $html .= '<ul class="treeview-menu">';

                $html .= $this->buildMenuTpl($v['children']);

                $html .= '</ul>';

            }

            $html .= '</li>';

        }

        return $html;

    }

}