<?php


namespace app\admin\controller;

use app\model\{AuthGroup,SysAdmin};
use form\FormBuilder as Form;
use think\facade\Url;
use traits\Admin;

class Admin
{

	use Admin;

    public static $description = "管理员管理";

	/**
	 * @title 管理员列表
	 * @throws \think\exception\DbException
	 */
    public function index()
    {
        if (self::$request->isAjax()) {

            $limit = $this->get['limit'] ?? 30;

            $data = SysAdmin::with('auth_group')->order('id desc')->paginate($limit)->each(function ($item) {

                if (empty($item['group_name']) && $item['group'] == 0){

                    $item['group_name'] = '超级管理员';

                }

                $item['handle'] = [
                    [
                        'name' => '修改',
                        'href' => Url::build('edit', ['id' => $item['id']]),
                        'event_type' => 'url',
                        'btn_type' => 'btn-success'
                    ],
                    [
                        'name' => '删除',
                        'href' => Url::build('destory', ['id' => $item['id']]),
                        'event_type' => 'confirm',
                        'btn_type' => 'btn-danger'
                    ]
                ];

            });

            return $this->layTable($data);

        } else {

            return $this->output();

        }
    }

    /**
     * @title 创建管理员
     */
    public function create()
    {

        if (self::$request->isPost()) {

            $rule = [
                'account' => 'require|length:3,50|unique:sys_admin',
                'password' => 'require|length:5,30',
                'nickname' => 'require'
            ];

            $msg = [
                'account.require' => '请填写管理员账号',
                'account.length' => '管理员账号字符长度必须为3至50之间',
                'account.unique' => '管理员账号已存在',
                'password.require' => '请填写管理员密码',
                'password.length' => '管理员密码字符长度必须为5至30之间',
                'nickname.require' => '请填写管理员昵称'
            ];

            $result = $this->validate(self::$post, $rule, $msg);

            if ($result !== true) {

                return $this->output($result, self::$validateErr);

            }

			self::$post['joinip'] = self::$request->ip();

			self::$post['password'] = password_hash(self::$post['password'], PASSWORD_BCRYPT);

            if (SysAdmin::create(self::$post)) {
                return $this->output('管理员创建成功');
            }

            return $this->output('管理员创建失败', self::$fail);

        } else {
            $field = [
                Form::input('account', '登录账号')->required('账号不能为空'),
                Form::input('password', '密码')->required('密码不能为空'),
                Form::input('nickname', '昵称')->required('昵称不能为空'),
                Form::selectOne('group','所属用户组')->setOptions(function (){

                    $label = [
                        [
                            'value' => 0,
                            'label' => '超级管理员'
                        ]
                    ];

                    $groups = AuthGroup::select();

                    foreach ($groups as $item){

                        array_push($label,[
                            'value' => $item['id'],
                            'label' => $item['name']
                        ]);

                    }

                    return $label;

                })->required('请选择用户组'),
                Form::uploadImageOne('avatar', '头像上传', Url::build('upload/image'))->uploadType('image')->name('img')->maxSize('5120'),
            ];

            $form = Form::make_post_form('添加管理员', $field, url('create'), 2);

            $this->import('form', $form);

            return $this->output(self::$formTpl);
        }

    }

	/**
	 * @title 编辑管理员
	 *
	 * @param int $id
	 *
	 * @return string|\think\response
	 */
    public function edit(int $id)
    {

        if (self::$request->isAjax()) {

            $rule = [
                'account' => 'require|length:3,50|unique:sys_admin,account,' . $id,
                'password' => 'length:5,30',
                'nickname' => 'require'
            ];
            $msg = [
                'account.require' => '请填写管理员账号',
                'account.length' => '管理员账号字符长度必须为3至50之间',
                'account.unique' => '管理员账号已存在',
                'password.length' => '管理员密码字符长度必须为5至30之间',
                'nickname.require' => '请填写管理员昵称'
            ];

            $result = $this->validate(self::$post, $rule, $msg);

            if ($result !== true) {
                return $this->output($result, self::$validateErr);
            }

            if (!empty(self::$post['password'])) {

				self::$post['password'] = password_hash(self::$post['password'], PASSWORD_BCRYPT);

            } else {

                unset(self::$post['password']);

            }

            if (SysAdmin::update(self::$post)) {

                return $this->output('保存成功');

            }

            return $this->output('保存失败', 0);

        }

        $admin = SysAdmin::get($id);

        $field = [
            Form::hidden('id', $admin->id),
            Form::input('account', '登录账号', $admin->account ?? '')->required('账号不能为空'),
            Form::input('password', '密码')->placeholder('不修改则不填写'),
            Form::input('nickname', '昵称', $admin->nickname ?? '')->required('昵称不能为空'),
            Form::selectOne('group','所属用户组',$admin['group'])->setOptions(function (){

                $label = [
                    [
                        'value' => 0,
                        'label' => '超级管理员'
                    ]
                ];

                $groups = AuthGroup::select();

                foreach ($groups as $item){

                    array_push($label,[
                        'value' => $item['id'],
                        'label' => $item['name']
                    ]);

                }

                return $label;

            })->required('请选择用户组'),
            Form::uploadImageOne('avatar', '头像上传', Url::build('upload/image'), $admin->avatar ?? '')->uploadType('image')->name('img')->maxSize('5120'),
        ];

        $form = Form::make_post_form('修改管理员', $field, Url::build('edit'), 2);

        $this->import('form', $form);

        return $this->output(self::$formTpl);

    }

	/**
	 * @title 删除管理员
	 *
	 * @param int $id
	 *
	 * @return string|\think\response
	 */
    public function destory(int $id)
    {

    	self::$api = true;

        $admin = SysAdmin::get($id);

        if (empty($admin)){
            return $this->output('管理员不存在', self::$fail);
        }

        $admin->delete();

        $this->import('url',Url::build('index'));

        return $this->output('删除成功');

    }

}