<?php
/**
 * 获取指定模块下所有控制器
 */
function getController ()
{
	$dir = \think\facade\Env::get('app_path') . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'controller';
	$list = glob($dir . '/*.php');
	$result = [];
	foreach ($list as $k => $v) {
		$result[] = basename($v, '.php');
	}
	return $result;
}

/**
 * 获取指定控制器下所有方法
 *
 * @param string 控制器
 * @param string 父控制器
 *
 * @return array
 */
function getActions ($controller)
{
	$controller = 'app\admin\controller' . '\\' . $controller;
	return array_diff(get_class_methods(new $controller()), [
		'__construct',
		'import',
		'output',
		'layTable',
		'success',
		'error',
		'getResponseType',
		'validate'
	]);
}

/**
 * 获取指定方法的注释
 *
 * @param string module
 *
 * @return bool|string
 */
function getActionNote ($controller, $action)
{
	$path = 'app\admin\controller\\' . $controller;
	$relation = new \ReflectionMethod(new $path(), $action);
	$tmp = $relation->getDocComment();
	preg_match_all('/@title(.*?)\n/', $tmp, $tmp);
	$tmp = trim($tmp[1][0]);
	$tmp = $tmp != '' ? $tmp : '未设置标题';
	return $tmp;
}