<?php

namespace app\admin\validate;

use think\Validate;

class Login extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
        'account'  => 'require|length:3,50',
        'password' => 'require|length:5,30'
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        'account.require'  => '登录账号不能为空',
        'account.length'   => '登录账号字符长度必须为3至50之间',
        'password.require' => '登录密码不能为空',
        'password.length'  => '登录密码字符长度必须为5至30之间',
    ];
}
