<?php


namespace app\model;


use think\Model;

class User extends Model
{

    protected $table = 'user';

    protected $pk = 'id';

    protected $type = [
        'create_time' => 'timestamp:Y-m-d H:i',
        'update_time' => 'timestamp:Y-m-d H:i',
        'login_time' => 'timestamp:Y-m-d H:i'
    ];

    protected $autoWriteTimestamp = true;

    public function parent()
    {
        return $this->belongsTo('User', 'parent_id')->bind([
            'parent_name' => 'nickname',
            'parent_mobile' => 'mobile'
        ]);
    }

    public function getAvatarAttr($v)
    {
        return $v ?: '/static/avatar.png';
    }

    public function getSexAttr($v)
    {
        $sex = [
            '未知',
            '男',
            '女'
        ];

        return $sex[$v];

    }

    /**
     * @param object $query
     * @param mixed $value
     * @param mixed $data
     */
    public function searchCreateTimeAttr($query, $value, $data)
    {
        $query->whereBetweenTime('create_time', $value[0], $value[1]);
    }

    /**
     * @param object $query
     * @param mixed $value
     * @param mixed $data
     */
    public function searchMobileAttr($query, $value, $data)
    {
        $query->where('mobile', $value);
    }

    /**
     * @param object $query
     * @param mixed $value
     * @param mixed $data
     */
    public function searchNicknameAttr($query, $value, $data)
    {
        $query->where('nickname', 'like', "{$value}%");
    }

}