<?php


namespace app\model;


use think\facade\Config;
use think\Model;

class SysMenu extends Model
{

    protected $table = 'sys_menu';

    protected $pk = 'id';

    protected $type = [
        'create_time' => 'timestamp:Y-m-d H:i',
        'upadte_time' => 'timestamp:Y-m-d H:i',
    ];

    protected $autoWriteTimestamp = true;

    /**
     * @note 获取菜单select列表
     */
    public static function getMenuSelect($fid = 0, &$result = [], $space = 0)
    {
        $space += 2;
        $menu = self::where('fid', $fid)->select()->toArray();
        foreach ($menu as $k => $v) {
            $v['title'] = str_repeat('_', $space) . '|--' . $v['title'];
            $result[] = $v;
            self::getMenuSelect($v['id'], $result, $space);
        }
        return $result;
    }

    /**
     * @param $group
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function buildMenu($group)
    {

        if ($group == 0) {

            $menu = self::field('id,title,icon,link,fid')->all()->toArray();

            $menu = self::getMenuList($menu);

            $rootMenu = Config::get('menu.');

            return array_merge([
                [
                    'title' => '系统设置',
                    'icon' => 'glyphicon glyphicon-asterisk',
                    'link' => '',
                    'children' => $rootMenu
                ]
            ], $menu);

        } else {

            $group = AuthGroup::findOrEmpty($group);

            $auth = SysAuth::where('id', 'in', $group['auth'])->field('id,controller,action,fid')->all()->toArray();

            $auth_crt = array_unique(array_column($auth, 'controller'));

            $menu = self::field('id,title,icon,link,fid')->where('controller', 'in', $auth_crt)->select()->toArray();

            $fid = array_column($menu, 'fid');

            $parentMenu = self::field('id,title,icon,link,fid')->where('id', 'in', $fid)->select()->toArray();

            $menu = array_merge($parentMenu, $menu);

            return self::getMenuList($menu);

        }

    }


    /**
     * @note 获取多级结构菜单数组
     */
    private static function getMenuList($data, $fid = 0)
    {
        $result = [];
        foreach ($data as $k => $v) {
            if ($v['fid'] == $fid) {
                $v['children'] = self::getMenuList($data, $v['id']);
                $result[] = $v;
            }
        }
        return $result;
    }


    /**
     * @note  验证该标题是否存在
     * @param string $title 菜单名称
     * @return bool
     */
    public static function valititle($title, $id = 0)
    {
        if ($id != 0) {
            $vali = self::get(function (object $query) use ($title, $id) {
                $query->where('title', $title);
                $query->where('id', 'neq', $id);
            });
        } else {
            $vali = self::get(['title' => $title]);
        }
        if ($vali) {
            return false;
        }
        return true;
    }

}