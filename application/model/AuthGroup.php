<?php


namespace app\model;


use think\Model;

class AuthGroup extends Model
{

    protected $table = 'auth_group';

    protected $pk = 'id';

    protected $type = [
        'create_time' => 'timestamp:Y-m-d H:i',
        'update_time' => 'timestamp:Y-m-d H:i',
    ];

    protected $autoWriteTimestamp = true;

    /**
     * @param object $query
     * @param mixed $value
     * @param mixed $data
     */
    public function searchCreateTimeAttr($query, $value, $data)
    {
        $query->whereBetweenTime('create_time', $value[0], $value[1]);
    }

    /**
     * @param object $query
     * @param mixed $value
     * @param mixed $data
     */
    public function searchNameAttr($query, $value, $data)
    {
        $query->where('name', $value);
    }

}