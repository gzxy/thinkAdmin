<?php


namespace app\model;


use think\Model;

class ExceptionLog extends Model
{

    protected $table = 'exception';

    protected $pk = 'id';

    protected $type = [
        'create_time' => 'timestamp:Y-m-d H:i',
        'upadte_time' => 'timestamp:Y-m-d H:i',
    ];

    protected $autoWriteTimestamp = true;

    public function setIpAttr($value)
    {
        return ip2long($value);
    }

    public function setParamAttr($value)
    {
        return json_encode($value);
    }

    public function getIpAttr($value)
    {
        return long2ip($value);
    }

    public function getParamAttr($value)
    {
        return json_decode($value, true);
    }

}