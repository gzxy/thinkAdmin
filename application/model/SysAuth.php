<?php


namespace app\model;

use think\Model;

class SysAuth extends Model
{

    protected $table = 'sys_auth';

    protected $pk = 'id';

    protected $type = [
        'create_time' => 'timestamp:Y-m-d H:i',
        'upadte_time' => 'timestamp:Y-m-d H:i',
    ];

    protected $autoWriteTimestamp = true;

    /**
     * @note  批量auth添加
     * @param array $data auth数据
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function buildMenu($data){
        foreach($data as $v){
            $auth = self::where('controller',$v['controller'])->where('fid',0)->find();

            if(!$auth){
                $parentid = self::insertGetId(['controller'=>$v['controller'],'desc'=>$v['desc']]);
            }else{
                $auth->desc = $v['desc'];
                $auth->save();
                $parentid = $auth->id;
            }
            if(is_array($v['children']) && count($v['children']) > 0){

                foreach($v['children'] as $vv){
                    $childAuth = self::where('controller',$vv['controller'])->where('action',$vv['action'])->where('fid',$parentid)->find();
                    if($childAuth){
                        $childAuth->desc = $vv['desc'];
                        $childAuth->save();
                    }else{
                        self::create(['controller'=>$vv['controller'],'action'=>$vv['action'],'desc'=>$vv['desc'],'fid'=>$parentid]);
                    }
                }
            }
        }
        return true;

    }

}