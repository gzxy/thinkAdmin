<?php

namespace app\model;

use think\Model;
use think\facade\{Cookie,Session};

class SysAdmin extends Model
{

    protected $table = 'sys_admin';

    protected $pk = 'id';

    protected $type = [
        'jointime' => 'timestamp:Y-m-d H:i',
        'logintime' => 'timestamp:Y-m-d H:i',
    ];

    protected $autoWriteTimestamp = true;

    protected $createTime = 'jointime';

    protected $updateTime = 'logintime';

    protected function setJoinipAttr($value)
    {
        return ip2long($value);
    }

    protected function getJoinipAttr($value)
    {
        return long2ip($value);
    }

    protected function setLoginipAttr($value)
    {
        return ip2long($value);
    }

    protected function getLoginipAttr($value)
    {
        return long2ip($value);
    }

    public function authGroup()
    {
        return $this->belongsTo('AuthGroup','group')->bind([
            'group_name' => 'name',
            'auth'
        ]);
    }

	/**
	 * @param      $data
	 * @param bool $keep
	 *
	 * @return array
	 * @throws \think\db\exception\DataNotFoundException
	 * @throws \think\db\exception\ModelNotFoundException
	 * @throws \think\exception\DbException
	 */
    public function login($data, $keep = true)
    {

        $admin = $this->get([
            'account' => $data['account']
        ]);

        if (empty($admin)) {
            return ['error', "账号或密码错误！", 'account/login'];
        }

        if (password_verify($data['password'], $admin->password) === false) {
            return ['error', "账号或密码错误！", 'account/login'];
        }

        $admin->token = uuid();

        $admin->loginip = request()->ip();

        if (password_needs_rehash($admin->password, PASSWORD_BCRYPT)) {

            $admin->password = password_hash($data['password'], PASSWORD_BCRYPT);

        }

        $admin->save();

        if ($keep) {
            Cookie::set('adminEncrypt', $admin->token, 2592000);
        }

        Session::set('admin', $admin->toArray());

        Session::set('menu', SysMenu::buildMenu($admin['group']));

        return [
            'success',
            "登录成功！",
            'index/index'
        ];

    }

    /**
     * @note  验证该账号是否存在
     * @param string $account 账号
     * @return bool
     */
    public static function valiAccount($account)
    {

        $admin = self::get(['account' => $account]);

        if ($admin) {
            return false;
        }
        return true;
    }

}