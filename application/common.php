<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

//唯一ID生成
function uuid()
{
    if (function_exists('uuid_create') === true) {
        return uuid_create(1);
    }
    $data = openssl_random_pseudo_bytes(16);
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

/**
 * 邀请码生成
 * @param $user_id
 * @return string
 */
function inviteCode($user_id)
{
    // 根据用户ID生成邀请码 这样既可保证邀请码的唯一性并且不用进行大量的递归查询
    static $source_string = 'E5FCDG3HQA4B1NOPJ2RSTUV67MWX89KLYZI';
    $code = '';
    while ($user_id > 0) {
        $mod = $user_id % 35;
        $user_id = ($user_id - $mod) / 35;
        $code = $source_string[$mod] . $code;
    }
    if (empty($code[3])) {
        $code = str_pad($code, 4, '0', STR_PAD_LEFT);
    }
    return $code;
}

/**
 * 根据邀请码获取用户真实ID
 * @param $code
 * @return float|int
 */
function getIdBycode($code)
{
    static $source_string = 'E5FCDG3HQA4B1NOPJ2RSTUV67MWX89KLYZI';
    if (strrpos($code, '0') !== false) {
        $code = substr($code, strrpos($code, '0') + 1);
    }
    $len = strlen($code);
    $code = strrev($code);
    $user_id = 0;
    for ($i = 0; $i < $len; $i++) {
        $user_id += strpos($source_string, $code[$i]) * pow(35, $i);
    }
    return $user_id;
}

//订单号生成
function order_sn()
{

    $year_code = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    $date_code = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'T', 'U', 'V', 'W', 'X', 'Y'];

    $order_sn = $year_code[(intval(date('Y')) - 2018) % 26] . //年 1位
        strtoupper(dechex(date('m'))) . //月(16进制) 1位
        $date_code[intval(date('d'))] . //日 1位
        substr(time(), -5) . substr(microtime(), 2, 5) . //秒 5位 // 微秒 5位
        sprintf('%02d', rand(0, 99)); // 随机数 2位

    return $order_sn;

}

/**
 * @param $string
 * @return bool|string
 */
function urlsafe_b64decode($string)
{
    $data = str_replace(['-', '_', ''], ['+', '/', '='], $string);
    $mod4 = strlen($data) % 4;
    if ($mod4) {
        $data .= substr('====', $mod4);
    }
    return base64_decode($data);
}

/**
 * @param $string
 * @return mixed|string
 */
function urlsafe_b64encode($string)
{
    $data = base64_encode($string);
    $data = str_replace(['+', '/', '='], ['-', '_', ''], $data);
    return $data;
}

/**
 * *计算某个经纬度的周围某段距离的正方形的四个点
 *
 * @param float lng 经度
 * @param float lat 纬度
 * @param float distance 该点所在圆的半径，该圆与此正方形内切，默认值为0.5KM
 * @return array 正方形的四个点的经纬度坐标
 * */
function SquarePoint($lng, $lat, $distance = 0.5)
{
    $dlng = 2 * asin(sin($distance / (2 * EARTH_RADIUS)) / cos(deg2rad($lat)));
    $dlng = rad2deg($dlng);

    $dlat = $distance / EARTH_RADIUS;
    $dlat = rad2deg($dlat);

    return [
        'left-top' => ['lat' => $lat + $dlat, 'lng' => $lng - $dlng],
        'right-top' => ['lat' => $lat + $dlat, 'lng' => $lng + $dlng],
        'left-bottom' => ['lat' => $lat - $dlat, 'lng' => $lng - $dlng],
        'right-bottom' => ['lat' => $lat - $dlat, 'lng' => $lng + $dlng]
    ];
}

/**
 * 求两个已知经纬度之间的距离,单位为米
 *
 * @param float $lng1 经度1
 * @param float $lat1 纬度1
 * @param float $lng2 经度2
 * @param float $lat2 纬度2
 * @return float 距离，单位米
 */
function get_distance(float $lng1, float $lat1, float $lng2, float $lat2): float
{
    // 将角度转为狐度
    $radLat1 = deg2rad($lat1); //deg2rad()函数将角度转换为弧度
    $radLat2 = deg2rad($lat2);
    $radLng1 = deg2rad($lng1);
    $radLng2 = deg2rad($lng2);
    $a = $radLat1 - $radLat2;
    $b = $radLng1 - $radLng2;
    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6378.137 * 1000;
    return $s;
}

/**
 * 把返回的数据集转换成Tree
 * @param array $list 要转换的数据集
 * @param string $pk 主键字段
 * @param string $pid 父级字段
 * @param string $child 子节点键名
 * @param int $root 层级
 * @return array
 */
function list_to_tree($list, $pk = 'id', $pid = 'pid', $child = 'children', $root = 0)
{
    // 创建Tree
    $tree = [];
    if (is_array($list) || is_object($list)) {
        // 创建基于主键的数组引用
        $refer = [];
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] =& $list[$key];
        }
        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId = $data[$pid];
            if ($root == $parentId) {
                $tree[] =& $list[$key];
            } else {
                if (isset($refer[$parentId])) {
                    $parent =& $refer[$parentId];
                    $parent[$child][] =& $list[$key];
                }
            }
        }
    }
    return $tree;
}

/**
 * 随机数生成
 * @param int $length
 * @return int
 */
function generate_code(int $length = 6)
{
    $min = pow(10, ($length - 1));
    $max = pow(10, $length) - 1;
    return rand($min, $max);
}

/**
 * 银行卡号Luhm校验
 * @param int $bank_card
 * @return bool
 */
function Luhm($bank_card)
{
    $n = 0;
    for ($i = strlen($bank_card); $i >= 1; $i--) {
        $index = $i - 1;
        //偶数位
        if ($i % 2 == 0) {
            $n += $bank_card[$index];
        } else {//奇数位
            $t = $bank_card[$index] * 2;
            if ($t > 9) {
                $t = (int)($t / 10) + $t % 10;
            }
            $n += $t;
        }
    }
    return ($n % 10) == 0;
}
