(function () {
    var editor = [];var editorUpload= '';

    function sendFile(file,url,ele){
        var formData = new FormData();
        formData.append("img", file);
        $.ajax({
            url: url,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (res) {
                if(res.code == 1){
                    var data = res.data.filePath
                    $("#"+ele).summernote('insertImage', data, function ($image) {
                        $image.attr('src', data);
                    });
                }
            }
        });
    }

    setTimeout(function(){
        editor.forEach(function(v,k){
            var parentElem = $("#"+v.ref).parent();
            $("#"+v.ref).hide();
            var options = v.props;
            options.lang = 'zh-CN'

            if(options.action != undefined && options.action!=''){
                var url = options.action

                options.callbacks = {}
                options.callbacks.onImageUpload = function(files){

                    sendFile( files[0],url,v.field);
                }
            }
            delete options.model
            delete options.elementId
            delete options.value
            delete options.action

            parentElem.append('<textarea  id="'+v.field+'">'+v.value+'</textarea>')
            $("#"+v.field).summernote(options);

        })
    },200)

    var getRule = function () {
        var rule = <?=json_encode($rule)?>;
        rule.forEach(function (c,i) {
            if(c.type == 'editor'){
                c.type="input"
                editor.push(c);
            }
            if((c.type == 'cascader' || c.type == 'tree') && Object.prototype.toString.call(c.props.data) == '[object String]'){
                if(c.props.data.indexOf('js.') === 0){
                    c.props.data = window[c.props.data.replace('js.','')];
                }
            }
        });
        return rule;
    },vm = new Vue;
    return function create(el,callback) {
        var formData = {};
        if(!el) el = document.body;
        $f = formCreate.create(getRule(),{
                el:el,
                form:<?=json_encode($form->getConfig('form'))?>,
				row:<?=json_encode($form->getConfig('row'))?>,
        upload:{
            onExceededSize:function (file) {
                vm.$Message.error(file.name + '超出指定大小限制');
            },
            onFormatError:function (file) {
                vm.$Message.error(file.name + '格式验证失败');
            },
            onError:function (error) {
                vm.$Message.error('上传失败,('+ error +')');
            },
            onSuccess:function (res) {
                if(res.code == 1){
                    return res.data.filePath;
                }else{
                    vm.$Message.error(res.msg);
                }
            }
        },
        //表单提交事件
        onSubmit: function (formData) {
            $f.submitStatus({loading: true});
            if(editor.length > 0){
               for(let i in editor){
                    formData[editor[i]['field']] = $("#"+editor[i]['field']).val();
                }
            }
            $.ajax({
                url:'<?=$form->getAction()?>',
                type:'<?=$form->getMethod()?>',
                dataType:'json',
                data:formData,
                success:function (res) {
                    if(res.code == 1){
                        vm.$Message.success(res.msg);
                        formCreate.formSuccess && formCreate.formSuccess(res,$f,formData);
						callback && callback(0,res,$f,formData);
                        $f.submitStatus({loading: false});

						//TODO 表单提交成功!
                    }else{
						vm.$Message.error(res.msg);
                        $f.btn.finish();
						callback && callback(1,res,$f,formData);
                        $f.submitStatus({loading: false});
						//TODO 表单提交失败
                    }
                },
                error:function () {
                    vm.$Message.error('表单提交失败');
                    $f.btn.finish();
                }
            });
        }
    });
        return formData;
    };
}());
