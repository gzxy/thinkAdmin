<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$form->getTitle()?></title>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
	<?=implode("\r\n",$form->getScript())?>
	<?=$form->getSuccessScript()?>
    <script src="/public/summer-note/lang/summernote-zh-CN.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
</head>
<body>
<script>
    formCreate.formSuccess = function(form,$r){
        <?=$form->getSuccessScript()?>
    };
	(function () {
		var create = <?=$form->formScript()?>
        create();
    })();
</script>
</body>
</html>
