<?php
/**
 * FormBuilder表单生成器
 * Author: xaboy
 * Github: https://github.com/xaboy/form-builder
 */

namespace FormBuilder\traits\form;


use FormBuilder\components\Editor;

/**
 * Class FormInputNumberTrait
 * @package FormBuilder\traits\form
 */
trait FormEditorTrait
{

    /**
     * @param $field
     * @param $title
     * @param null $value
     * @return InputNumber
     */
    public static function editor($field, $title, $value = null,$action='')
    {
        return new Editor($field, $title, $value,$action);
    }
}
