<?php
/**
 * FormBuilder表单生成器
 * Author: xaboy
 * Github: https://github.com/xaboy/form-builder
 */

namespace FormBuilder\components;


use FormBuilder\FormComponentDriver;
use FormBuilder\Helper;

/**
 * summernote编辑器组建
 * Class Editor
 * @package FormBuilder\components
 * @method $this height(float $max) 高度
 * @method $this minHeight(float $min) 最小高度
 * @method $this maxHeight(float $max) 最大高度
 * @method $this placeholder(String $placeholder) 占位文本
 * @method $this focus(bool $focus) 是否自动聚焦
 */
class Editor extends FormComponentDriver
{
    /**
     * @var string
     */
    protected $name = 'editor';

    /**
     *
     */
    const TYPE_TEXTAREA = 'textarea';

    /**
     * @var array
     */
    protected $props = [
        'type' => self::TYPE_TEXTAREA,
    ];

    /**
     * @var array
     */
    protected static $propsRule = [
        'action'=>'string',
        'height' => 'float',
        'minHeight' => 'float',
        'maxHeight' => 'float',
        'focus' => 'bool',
        'placeholder' => 'string',
    ];



    /**
     *
     */
    protected function init()
    {
        $this->placeholder('请输入' . $this->title);
    }


    /**
     * 组件的值为必填
     * @param string $message
     * @return $this
     */
    public function required($message = null, $trigger = 'blur')
    {
        parent::setRequired(Helper::getVar($message, $this->getProps('placeholder')), $trigger);
        return $this;
    }

    /**
     * @return array
     */
    public function build()
    {

        return [
            'type' => $this->name,
            'field' => $this->field,
            'title' => $this->title,
            'value' => $this->value ,
            'props' => (object)$this->props,
            'validate' => $this->validate,
            'editor'=>true,
        ];
    }

}
