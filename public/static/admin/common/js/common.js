$(function () {
    let winh = $(document).height();

    $(".content iframe").attr('height', winh - 160 + 'px');

    $(document).on('click', 'button,span,a', function () {
        let data = $(this).data();
        let obj = Object.keys(data);
        if (obj.length > 0) {
            if (obj.includes('url') && data.url !== undefined) {
                parent.layer.open({
                    type: 2,
                    title: $(this).text(),
                    shadeClose: true,
                    shade: 0.8,
                    area: ['80%', '90%'],
                    content: data.url
                });
                return false;
            }
            if (obj.includes('href') && data.href !== undefined) {
                $.post(data.href, function (res) {
                    if (res.code === 1) {
                        swal(res.msg, "", "success");
                    } else {
                        swal(res.msg, "", "error");
                    }
                });
                return false
            }
            if (obj.includes('confirm') && data.confirm !== undefined) {
                let msg = obj.includes('msg') && data.confirm !== undefined ? data.msg : "确定执行操作吗?";
                swal({
                    text: "请谨慎操作!",
                    icon: "warning",
                    title: msg,
                    buttons: ['取消', '确定'],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.post(data.confirm, function (res) {
                            if (res.code === 1) {
                                swal(res.msg, {
                                    icon: "success",
                                });
                                setTimeout(function () {
                                    if (res.data.hasOwnProperty('url')) {
                                        location.href = res.data.url;
                                    } else {
                                        location.reload();
                                    }
                                }, 2000)
                            } else {
                                swal(res.msg, {
                                    icon: "error",
                                });
                            }
                        });
                    }
                });
            }
        }
    });

});

function send_get_request(url, data) {
    $.post(url, data, function (res) {
        if (res.code === 1) {
            swal(res.msg, "", "success");
            if (res.url) {
                setTimeout(function () {
                    location.href = res.url;
                }, 2000)
            }
        } else {
            swal(res.msg, "", "error");
        }
    });
}