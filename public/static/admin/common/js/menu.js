$(function(){
    var menu = [];

    $(".sidebar-menu li a").on('click',function(){
        var frame = $(this).data('frame');

        if(frame != undefined && frame != ''){
            var winh = $(document).height();
            var frameh = winh - 140+'px';
            var text = $(this).text();

            $("#content iframe").hide();
            $("#tab ul li").each(function(){
                $(this).attr("class","");
                $(this).find(".icon-group").remove();
            });

            var router = $(this).data('router');
            if(!menu.includes(router)){

                menu.push(router);

                if(menu.length > 7){
                    $("#tab ul li").eq(0).remove();
                    menu.splice(0,1);
                }

                $("#tab ul").append('<li class="active skin-btn" data-frameid="'+router+'"><span class="pull-left" style="padding-right: 5px">'+text+'</span> <div class="icon-group"><i data-icon="refresh" class="fa fa-refresh" aria-hidden="true"></i><i data-icon="close" class="fa fa-close  ml-2" aria-hidden="true"></i></div></li>');


                $('<iframe id="'+router+'"  width="100%" height="'+frameh+'" style="background: #ecf0f5" frameborder="0"   src="'+frame+'"> </iframe>').prependTo('#content');
            }else{
                $("#tab ul li").each(function(){
                    var frameid = $(this).data('frameid');

                    if(frameid == router){
                        $(this).attr("class","active skin-btn");
                        $(this).append('<div class="icon-group"><i data-icon="refresh" class="fa fa-refresh" aria-hidden="true"></i><i data-icon="close" class="fa fa-close  ml-2" aria-hidden="true"></i></div>')
                    }else{
                        return true;
                    }

                });
                $("#"+router).show();
            }
        }
    });

    $(document).on('click',"#tab ul li ",function(){
        var frame = $(this).data('frameid');
        $("#content iframe").hide();
        $("#tab ul li").each(function(){
            $(this).attr("class","");
            $(this).find(".icon-group").remove();
        });
        $("#"+frame).show();
        $(this).addClass('active skin-btn');
        $(this).append('<div class="icon-group"><i data-icon="refresh" class="fa fa-refresh" aria-hidden="true"></i><i data-icon="close" class="fa fa-close  ml-2" aria-hidden="true"></i></div>');
    });


    $(document).on('click',"#tab ul li .icon-group i",function(e){
        e.stopPropagation();  //阻止冒泡
        var icon = $(this).data('icon');
        var frameid = $(this).parent().parent().data('frameid');

        switch (icon) {
            case 'refresh':
                var ele = $(this);
                ele.addClass('fa-spin');
                document.getElementById(frameid).contentWindow.location.reload(true);
                setTimeout(function(){
                    ele.removeClass('fa-spin');
                },500);
                break;
            case 'close':
                $(this).parent().parent().remove();
                $("#"+frameid).remove();
                for(let i in menu){
                    if(menu[i] == frameid) {
                        menu.splice(i,1);
                        break;
                    }
                }
                if(menu.length == 0){
                    var router = "main-frame";
                }else{
                    var router = menu[0];
                    var elem =  $("#tab ul li[data-frameid='" + router + "']")
                    elem.addClass('active skin-btn');
                    elem.append('<div class="icon-group"><i data-icon="refresh" class="fa fa-refresh" aria-hidden="true"></i><i data-icon="close" class="fa fa-close  ml-2" aria-hidden="true"></i></div>');
                }
                $("#"+router).show();

                break;
            default:
                break;
        }
    });


    $("#search-menu").on("click",function(){

        var txt = $("input[name='search-menu']").val();

        // $("#sidebar-menu .treeview").addClass('menu-open');
        // $("#sidebar-menu .treeview-menu").show();


    });

});
