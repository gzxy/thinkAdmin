<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?=$form->getTitle()?></title>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <?=implode("\r\n",$form->getScript())?>
    <script src="/static/summer-note/lang/summernote-zh-CN.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

    <style>
        /*弹框样式修改*/
        .ivu-modal-body{padding: 5;}
        .ivu-modal-confirm-footer{display: none;}
    </style>
</head>
<body>
<script>

    formCreate.formSuccess = function(form,$r){
        <?=$form->getSuccessScript()?>
    };

    (function () {
        var create = <?=$form->formScript()?>
            create();
    })();


</script>
</body>
</html>

