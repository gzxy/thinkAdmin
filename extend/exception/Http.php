<?php

namespace exception;


use think\facade\{Response, Request};
use app\model\ExceptionLog;
use think\exception\Handle;
use Exception;

class Http extends Handle
{

    public function render(Exception $e)
    {
        //记录错误信息
        if ($e instanceof Exception) {
            ExceptionLog::create([
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
                'code' => $e->getCode(),
                'host' => Request::host(),
                'scheme' => Request::scheme(),
                'port' => Request::port(),
                'url' => Request::url(),
                'method' => Request::method(),
                'ip' => Request::ip(),
                'param' => Request::param()
            ]);
        }
        if (Request::isAjax() || Request::isPost()) {
            return Response::create([
                'code' => 0,
                'msg' => "服务器内部错误"
            ], 'json', 200);
        }
        return parent::render($e);
    }

}