<?php

namespace sms;

use think\facade\Validate;
use think\Loader;

/**
 * Class Sms
 * @package sms
 * @author Gzxy
 */
class Sms
{

    private $device = "Mysql";

    /**
     * 如果安装了redis 则将驱动转为Redis
     */
    public function __construct()
    {
        if (extension_loaded('redis')) {
            $this->device = 'Redis';
        }
    }

    /**
     * 发送短信
     *
     * @param array $options 配置参数 参照注释
     * @param string $service 接口提供方 暂只支持聚合（juhe）阿里（dayu）测试（test）
     * @param int $expire_time 验证码过期时间
     * @return array
     * @author Gzxy
     */
    public function send_sms(array $options, string $service, int $expire_time): array
    {
        /**
         *  聚合
         *  [
         *       'mobile' => '手机号',
         *       'tpl' => '短信模板ID',
         *       'key' => '聚合密钥',
         *       'data' => [
         *           //短信数据
         *           'code' => 123
         *       ]
         *  ]
         */
        /**
         *  阿里短信
         *  [
         *       'mobile' => '手机号',
         *       'tpl' => '短信模板ID',
         *       'accessKeyId' => 'Your accessKeyId',
         *       'accessKeySecret' => 'Your accessKeySecret',
         *       'SignName' => 'Your SignName',
         *       'data' => [
         *           //短信数据
         *           'code' => 123
         *       ]
         *  ]
         */

        [
            $code,
            $msg
        ] = self::validate($service, $options);

        if ($code != 1) {

            return [
                $code,
                $msg
            ];

        }

        $device = Loader::factory($this->device, '\\sms\\device\\', $service);

        $result = $device->sendSms($options, $expire_time);

        return [
            $result === true ? 1 : 0,
            $result === true ? "发送成功" : $device->getError()
        ];
    }

    /**
     * @param string $mobile 手机号
     * @param string $service 接口提供方 暂只支持聚合（juhe）阿里（dayu）测试（test）
     * @param string $tpl 模板ID
     * @param string $field 验证码所在字段
     * @param string $code 验证码数据
     * @return array
     * @author Gzxy
     */
    public function validateCcode(string $mobile, string $service, string $tpl, string $field, string $code): array
    {
        $device = Loader::factory($this->device, '\\sms\\device\\', $service);
        $result = $device->validateCode($mobile, $tpl, $field, $code);
        return [
            $result === true ? 1 : 0,
            $result === true ? '验证码正确' : $device->getError()
        ];
    }

    /**
     * @param string $service
     * @param array $options
     * @return array
     * @author Gzxy
     */
    private static function validate(string $service, array $options): array
    {

        $rule = [
            'mobile|手机号' => 'require|mobile',
            'tpl|短信模板' => 'require',
            'data|短信数据' => 'require',
        ];

        switch ($service) {
            case 'juhe':
                $rule['key|密钥'] = 'require';
                break;
            case 'dayu':
                $rule['accessKeyId'] = 'require';
                $rule['accessKeySecret'] = 'require';
                $rule['SignName'] = 'require';
                break;
        }

        $validate = Validate::rule($rule);

        $result = $validate->check($options);

        if (!$result){

            return [
                0,
                $validate->getError()
            ];

        }

        return [
            1,
            '验证成功'
        ];

    }

}