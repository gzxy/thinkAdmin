<?php

namespace sms\device;

use think\facade\Config;
use AlibabaCloud\Client\AlibabaCloud;

class Redis
{

    private $error;

    private $service;

    private $conn;

    public function __construct($service)
    {
        $redisConf = Config::get('redis.');
        $this->conn = new \Redis();
        $this->conn->connect($redisConf['host'], $redisConf['port']);
        if (isset($redisConf['auth']) && !empty($redisConf['auth'])) {
            $this->conn->auth($redisConf['auth']);
        }
        if (isset($redisConf['db']) && !empty($redisConf['db'])) {
            $this->conn->select($redisConf['db']);
        } else {
            $this->conn->select(0);
        }
        $this->service = $service;
    }

    /**
     * @param array $options
     * @param int $expire_time
     * @return bool
     * @author Gzxy
     */
    public function sendSms(array $options, int $expire_time): bool
    {
        $key = "sms:{$options['mobile']}:{$this->service}:{$options['tpl']}";

        $exists = $this->conn->exists($key);

        if ($exists && $this->service != 'test') {
            $over_time = $this->conn->ttl($key);
            $this->error = "还有{$over_time}秒可以发送";
            return false;
        }

        return $this->{$this->service}($options, $expire_time);
    }

    /**
     * @param string $mobile
     * @param string $tpl
     * @param string $field
     * @param string $code
     * @return bool
     * @author Gzxy
     */
    public function validateCode(string $mobile, string $tpl, string $field, string $code): bool
    {
        $key = "sms:{$mobile}:{$this->service}:{$tpl}";

        if (!$this->conn->exists($key)) {
            $this->error = '验证码错误';
            return false;
        }

        $log = $this->conn->get($key);

        $sms_data = unserialize($log);

        if ($sms_data && $sms_data[$field] == $code) {
            $this->conn->del($key);
            return true;
        }

        $this->error = '验证码错误';

        return false;

    }

    /**
     * @return string|null
     * @author Gzxy
     */
    public function getError(): ?string
    {
        return $this->error ?? null;
    }

    /**
     * @param array $options
     * @param int $expire_time
     * @return bool
     * @author Gzxy
     */
    private function test(array $options, int $expire_time): bool
    {
        return $this->storage($options['mobile'], $options['data'] ?? [], $options['tpl'] ?? 'test', $expire_time ?? 120);
    }

    /**
     * @param array $options
     * @param int $expire_time
     * @return bool
     * @author Gzxy
     */
    private function juhe(array $options, int $expire_time): bool
    {
        $url = "http://v.juhe.cn/sms/send";

        $tpl_value = '';

        $i = 0;

        foreach ($options['data'] as $k => $v) {
            $i += 1;
            if ($i > 1) {
                $tpl_value .= "&";
            }
            $tpl_value .= "#{$k}#={$v}";
        }

        $params = [
            'key' => $options['key'],
            'mobile' => $options['mobile'],
            'tpl_id' => $options['tpl'],
            'tpl_value' => $tpl_value
        ];

        $res = self::curlRequest($url, $params);

        $res = json_decode($res, true);

        if ($res === null) {
            $this->error = "请求错误";
            return false;
        }

        if ($res['error_code'] == 0) {
            return $this->storage($options['mobile'], $options['data'], $options['tpl'], $expire_time);
        }

        $this->error = $res['reason'];

        return false;

    }

    /**
     * @param array $options
     * @param int $expire_time
     * @return bool
     * @author Gzxy
     */
    private function dayu(array $options, int $expire_time): bool
    {
        try {
            AlibabaCloud::accessKeyClient($options['accessKeyId'], $options['accessKeySecret'])
                ->regionId('cn-hangzhou')
                ->asDefaultClient();
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }

        try {
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->options([
                    'query' => [
                        'RegionId' => "cn-hangzhou",
                        'PhoneNumbers' => $options['mobile'],
                        'SignName' => $options['SignName'],
                        'TemplateCode' => $options['tpl'],
                        'TemplateParam' => json_encode($options['data']),
                    ],
                ])
                ->request();
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }

        $result = $result->toArray();

        if ($result['Code'] == 'OK') return $this->storage($options['mobile'], $options['data'], $options['tpl'], $expire_time);

        $this->error = $result['Message'];

        return false;
    }

    /**
     * @param string $mobile
     * @param array $data
     * @param string $tpl
     * @param int $expire_time
     * @return bool
     * @author Gzxy
     */
    public function storage(string $mobile, array $data, string $tpl, int $expire_time): bool
    {
        $result = $this->conn->set("sms:{$mobile}:{$this->service}:{$tpl}", serialize($data), $expire_time);
        if ($result) return true;
        $this->error = "服务器内部错误";
        return false;
    }

    /**
     * 公共请求curl方法
     * @param string $url 访问url
     * @param array $data 携带参数
     * @param string $method 请求类型 默认post
     * @return mixed 请求返回数据
     * @author Gzxy
     */
    private static function curlRequest(string $url, array $data, string $method = "post")
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($method == "post") {
            //POST请求的参数
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        //忽略https的安全证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        $rs = curl_exec($ch);
        curl_close($ch);
        return $rs;
    }

    public function __destruct()
    {
        $this->conn->close();
    }

}