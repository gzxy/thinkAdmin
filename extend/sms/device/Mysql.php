<?php

namespace sms\device;

use think\Db;
use AlibabaCloud\Client\AlibabaCloud;

class Mysql
{

    private $error;

    private $service;

    public function __construct($service)
    {
        $where = [
            'expire_time' => [
                'lt',
                time()
            ],
            'status' => 0
        ];
        Db::table('sms_logs')->where($where)->update([
            'status' => 2
        ]);
        $this->service = $service;
    }

    /**
     * @param array $options
     * @param int $expire_time
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author Gzxy
     */
    public function sendSms(array $options, int $expire_time): bool
    {
        $expire = Db::table('sms_logs')->where('mobile', $options['mobile'])->where('expire_time', 'gt', time())->findOrEmpty();

        if ($expire && $this->service != 'test') {

            $expire_time = $expire['expire_time'] - time();

            $this->error = "还有{$expire_time}秒可以发送";

            return false;

        }

        return $this->{$this->service}($options, $expire_time);
    }

    /**
     * @param string $mobile
     * @param string $tpl
     * @param string $field
     * @param string $code
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @author Gzxy
     */
    public function validateCode(string $mobile, string $tpl, string $field, string $code): bool
    {
        $where = [
            'mobile' => $mobile,
            'service' => $this->service,
            'tpl' => $tpl,
            'expire_time' => ['gt', time()],
            'status' => 0,
        ];

        $log = Db::table('sms_log')->where($where)->findOrEmpty();

        if (empty($log)) {
            $this->error = '验证码错误';
            return false;
        }

        $sms_data = unserialize($log['data']);

        if ($sms_data && $sms_data[$field] == $code) {
            Db::table('sms_log')->where('id', $log['id'])->update([
                'status' => 1,
                'update_time' => time()
            ]);
            return false;
        }

        $this->error = '验证码错误';

        return false;

    }

    /**
     * @return string|null
     * @author Gzxy
     */
    public function getError(): ?string
    {
        return $this->error ?? null;
    }

    /**
     * @param array $options
     * @param int $expire_time
     * @return bool
     * @author Gzxy
     */
    private function test(array $options, int $expire_time): bool
    {
        return $this->storage($options['mobile'], $options['data'] ?? [], $options['tpl'] ?? '', $expire_time ?? 120);
    }

    /**
     * @param array $options
     * @param int $expire_time
     * @return bool
     * @author Gzxy
     */
    private function juhe(array $options, int $expire_time): bool
    {
        $url = "http://v.juhe.cn/sms/send";

        $tpl_value = '';

        $i = 0;

        foreach ($options['data'] as $k => $v) {
            $i += 1;
            if ($i > 1) {
                $tpl_value .= "&";
            }
            $tpl_value .= "#{$k}#={$v}";
        }

        $params = [
            'key' => $options['key'],
            'mobile' => $options['mobile'],
            'tpl_id' => $options['tpl'],
            'tpl_value' => $tpl_value
        ];

        $res = self::curlRequest($url, $params);

        $res = json_decode($res, true);

        if ($res === null) {
            $this->error = "请求错误";
            return false;
        }

        if ($res['error_code'] == 0) {
            return $this->storage($options['mobile'], $options['data'], $options['tpl'], $expire_time);
        }

        $this->error = $res['reason'];

        return false;

    }

    /**
     * @param array $options
     * @param int $expire_time
     * @return bool
     * @author Gzxy
     */
    private function dayu(array $options, int $expire_time): bool
    {
        try {
            AlibabaCloud::accessKeyClient($options['accessKeyId'], $options['accessKeySecret'])
                ->regionId('cn-hangzhou')
                ->asDefaultClient();
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }

        try {
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->options([
                    'query' => [
                        'RegionId' => "cn-hangzhou",
                        'PhoneNumbers' => $options['mobile'],
                        'SignName' => $options['SignName'],
                        'TemplateCode' => $options['tpl'],
                        'TemplateParam' => json_encode($options['data']),
                    ],
                ])
                ->request();
        } catch (\Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }

        $result = $result->toArray();

        if ($result['Code'] == 'OK') return $this->storage($options['mobile'], $options['data'], $options['tpl'], $expire_time);

        $this->error = $result['Message'];

        return false;
    }

    /**
     * @param string $mobile
     * @param array $data
     * @param string $tpl
     * @param int $expire_time
     * @return bool
     * @author Gzxy
     */
    public function storage(string $mobile, array $data, string $tpl, int $expire_time): bool
    {
        $logs = [
            'mobile' => $mobile,
            'data' => serialize($data),
            'tpl' => $tpl,
            'service' => $this->service,
            'create_time' => time(),
            'expire_time' => time() + $expire_time,
        ];
        if (Db::table('sms_logs')->insert($logs)) {
            return true;
        }
        $this->error = "服务器内部错误";
        return false;
    }

    /**
     * 公共请求curl方法
     * @param string $url 访问url
     * @param array $data 携带参数
     * @param string $method 请求类型 默认post
     * @return mixed 请求结果
     * @author Gzxy
     */
    private static function curlRequest(string $url, array $data, string $method = "post")
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($method == "post") {
            //POST请求的参数
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        //忽略https的安全证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        $rs = curl_exec($ch);
        curl_close($ch);

        return $rs;
    }

}