<?php

namespace form;

use FormBuilder\Form;

class FormBuilder extends Form
{

    public static function make_post_form($title, array $field, $url, $js_callback = 2, $label = 150)
    {
        $form = Form::create($url);
        $form->setMethod('POST');
        $form->components($field);
        $form->setTitle($title);
        $form->formStyle(Form::style()->labelWidth($label)->autocomplete(true));
        switch ($js_callback) {
            case 1:
                //提交成功当前页面刷新
                $js = <<<JS
                    setTimeout(function(){location.reload()},2000)
JS;
                break;
            case 2:
                //提交成功父级页面刷新并关闭当前页面
                $js = <<<JS
                    let index = parent.layer.getFrameIndex(window.name);
                    setTimeout(function(){
                        parent.layer.close(index);
                        let iframeid=parent.$("#tab>ul>li.active");
                        if(iframeid){
                            parent.document.getElementById(iframeid.data("frameid")).contentWindow.location.reload();
                        }
                    },1500)
JS;
                break;
            case 3:
                //提交成功关闭表单页面并刷新主页面
                $js = <<<JS
                    let index = parent.layer.getFrameIndex(window.name);
                    setTimeout(function(){
                        parent.layer.close(index);
                        parent.location.reload();
                    },1500)
JS;
                break;
            default:
                $js = $js_callback;
                break;
        }
        $form->setSuccessScript($js);//提交成功执行js
        return $form;
    }

}