<?php
namespace facade;

use think\Facade;

/**
 * @see \doc\Document
 * @method \doc\Document options(array $options = []) static 设置文档生成参数
 * @method array makeDocRule(string $path = '') static 生成文档数据
 */
class Document extends Facade
{

    protected static function getFacadeClass()
    {
        return 'doc\Document';
    }

}