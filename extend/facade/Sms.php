<?php


namespace facade;

use think\Facade;

/**
 * @see \sms\Sms
 * @method array sendSms(array $options,string $service,int $expire_time) static 发送短信验证码
 * @method array validateCode(string $mobile,string $service,string $tpl,string $field,string $code) static 验证短信验证码
 */
class Sms extends Facade
{

    protected static function getFacadeClass()
    {
        return 'sms\Sms';
    }

}