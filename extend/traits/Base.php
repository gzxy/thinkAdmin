<?php

namespace traits;

use think\facade\{Response, Request, View};
use think\Container;

trait Base
{
    private static $result = [];                    //最终返回结果集

    private static $api = true;                     //是否为api接口

    private static $get = [];                       //GET请求参数

    private static $post = [];                      //POST请求参数

    private static $route = [];                     //路由请求参数

    private static $timestamp = 0;                  //当前时间戳

    private static $u;                              //当前用户对象

    private static $request;                        //当前请求实例

    public function __construct()
    {
        self::$request = Request::instance();

        self::$post = self::$request->post();

        self::$get = self::$request->get();

        self::$route = self::$request->route();

        self::$timestamp = self::$request->time();

        self::$u = self::$request->u ?? false;
    }

    /**
     * @param array|string$name
     * @param mixed $value
     * @return $this
     * @author gzxy
     */
    private function import($name, $value = '')
    {
        if (self::$api === false) {

            View::assign($name, $value);

        } else {

            if (is_array($name)) {

                self::$result = array_merge(self::$result, $name);

            } else {

                self::$result[$name] = $value;

            }

        }

        return $this;
    }

    /**
     * @return string|\think\response
     * @author gzxy
     */
    private function output()
    {

        $args = func_get_args();

        if (self::$api === true) {

            $msg = $args[0] ?? '';

            $code = $args[1] ?? 1;

            return Response::create([
                'code' => $code,
                'msg' => $msg,
                'data' => self::$result
            ], 'json', 200);

        }

        $template = $args[0] ?? '';

        $vars = $args[1] ?? [];

        $config = $args[2] ?? [];

        return View::fetch($template, $vars, $config);

    }

    /**
     * @param $data
     * @param $validate
     * @param array $message
     * @param bool $batch
     * @param null $callback
     * @return bool
     * @author gzxy
     */
    private function validate($data, $validate, $message = [], $batch = false, $callback = null)
    {
        $app = Container::get('app');

        if (is_array($validate)) {
            $v = $app->validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                list($validate, $scene) = explode('.', $validate);
            }
            $v = $app->validate($validate);
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }

        // 是否批量验证
        if ($batch) {
            $v->batch(true);
        }

        if (is_array($message)) {
            $v->message($message);
        }

        if ($callback && is_callable($callback)) {
            call_user_func_array($callback, [
                $v,
                &$data
            ]);
        }

        if (!$v->check($data)) {
            return $v->getError();
        }

        return true;

    }

    public function __toString()
    {
        return Response::create(self::$result, 'json', 200);
    }

}
