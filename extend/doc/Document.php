<?php

namespace doc;

use Exception;

class Document
{

    private $options = [
        'api_dir' => '../api/controller',
        'project_name' => '牛云科技文档生成',
        'host' => 'http://127.0.0.1',
        'ignore' => [
            '.',
            '..',
            'Doc.php',
            'Base.php'
        ]
    ];

    private $docArr = [];

    public function options(array $options = []): Document
    {
        $this->options = array_merge($this->options, $options);
        return $this;
    }

    public function makeDocRule(string $path = null): array
    {

        $path = $path ?? $this->options['api_dir'];

        if (!is_dir($path)) {
            throw new Exception("{$path}不是一个文件夹", 0);
        }

        $handler = opendir($this->options['api_dir']);

        while (($filename = readdir($handler)) !== false) {

            if (in_array($filename, $this->options['ignore'])) {
                continue;
            }

            if (is_dir($path . DIRECTORY_SEPARATOR . $filename)) {
                $this->makeDocRule($path . DIRECTORY_SEPARATOR . $filename);
            } else {
                $docData = $this->complie($path . DIRECTORY_SEPARATOR . $filename);
                if (!empty($docData)) {
                    $this->docArr[$filename] = $docData;
                }
            }
        }

        closedir($handler);

        return array_merge($this->options,[
            'doc' => $this->docArr
        ]);

    }

    private function complie(string $filename)
    {
        $fileData = [];

        $file = fopen($filename, 'r');

        if (filesize($filename) <= 0) return false;

        $string = fread($file, filesize($filename));

        if (empty($string)) return $fileData;

        preg_match_all('/\/\*\*([\s\S]+)\*\//U', $string, $regexp);

        if (empty($regexp[1])) {
            return $fileData;
        }

        foreach ($regexp[1] as $key => $value) {

            $arr = preg_split('/\*/', $value);
            $newArr = ['params' => [], 'return' => [], 'plus' => []];

            if (empty($arr)) return $fileData;

            foreach ($arr as $item) {

                if ($item) {

                    $itemArr = preg_split('/\s+/', trim($item));

                    foreach ($itemArr as $k => $v) {

                        //apiName
                        $length = count($itemArr);

                        if ($length === 1) {
                            $newArr['apiName'] = $v;
                        }
                        //url
                        if ($length === 2 && $itemArr[0] === '@url' && $k === 0) {
                            $newArr['url'] = $itemArr[1];
                        }
                        //method
                        if ($length === 2 && $itemArr[0] === '@method' && $k === 0) {
                            $newArr['method'] = $itemArr[1];
                        }
                        //plus
                        if ($length >= 4 && $itemArr[0] === '@plus' && $k === 0) {
                            array_push($newArr['plus'], [
                                'type' => $itemArr[1],
                                'name' => preg_replace('/\$/', '', $itemArr[2]),
                                'msg' => $itemArr[3],
                                'default' => isset($itemArr[4]) ? $itemArr[4] : '',
                            ]);
                        }
                        //params
                        if ($length >= 4 && $itemArr[0] === '@param' && $k === 0) {
                            array_push($newArr['params'], [
                                'type' => $itemArr[1],
                                'name' => preg_replace('/\$/', '', $itemArr[2]),
                                'msg' => $itemArr[3],
                                'default' => isset($itemArr[4]) ? $itemArr[4] : '',
                            ]);
                        }
                        //return
//                        if ($length > 1 && $itemArr[0] === '@return' && $k === 0) {
//                            array_push($newArr['return'], [
//                                'name' => preg_replace('/\$/', '', $itemArr[1]),
//                                'msg' => $itemArr[2],
//                            ]);
//                        }
                    }

                }
            }
            if (isset($newArr['method']) && !empty($newArr['method'])) {
                array_push($fileData, $newArr);
            }

        }

        fclose($file);

        return $fileData;
    }

}